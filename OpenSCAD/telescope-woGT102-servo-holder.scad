$fn = 150;
focuserDiameter = 88; // 88mm
boltDiameter = 4.75; // 4.75mm 3/16 in
boltLength = 6.33; // 6.33mm 1/4 in
focLengthToBlackBar = 36.50; //36.57mm  1 7/16 in
boltToGoldRing = 3; // 3mm  1/8 in

centerBoltToServoMount = 60; //60mm
blackBarWidth = 90;

// From the top of the focuser to servo
bottomServo =10;
topServo = bottomServo + 15;
edgeServoFromBolt = 23;



// This module is used for creating wedges
module prism(l, w, h) {
	translate([0, l, 0]) rotate( a= [90, 0, 0]) 
	linear_extrude(height = l) polygon(points = [
		[0, 0],
		[w, 0],
		[0, h]
	], paths=[[0,1,2,0]]);
}


/*
Main mounting block with shaved off areas to
account for the focuser tube, and to save weight.  
Also a hole for the bolt is added along with a 
countersink hole for the bolt head.
*/
difference() { 
    // Main mounting block
    cube([blackBarWidth,focLengthToBlackBar,15]);
    // Remove focuser tube area
    translate([blackBarWidth/2,70,-32]) {
        rotate(90, [1,0,0])
        cylinder(r=focuserDiameter/2, h=80);
    }
    //Bolt hole
    translate([blackBarWidth/2,boltDiameter/2+3,5]) {
        cylinder(r=boltDiameter/2, h=20);
    }
    // Counter sink for the bolt hole
    translate([blackBarWidth/2,boltDiameter/2+3,14]) {
        cylinder(r=10.5/2, h=2);
    }
    
    // Trim off the excess from the side
    translate([0,0,20]) {
        rotate([0,90,0]) 
        prism(blackBarWidth/2,30,30);
    }

}

// Top bar that holds the servo
difference() {
    // Top bar
    translate([blackBarWidth/2,3,15]) { 
        cube([centerBoltToServoMount,edgeServoFromBolt,10-3]);
    }
    // The area around the bolt and countersink area is trimmed off here.
    translate([blackBarWidth/2,3,14]) {
        cylinder(r=16/2, h=20);
    }
    
}

// Extra piece added to top bar for additional support
difference() {
    // Extra top bar piece
    translate([blackBarWidth/2,0,15]) { 
        cube([centerBoltToServoMount,3,10-3]);
    }
    // The area around the bolt and countersink area is trimmed off here.
    translate([blackBarWidth/2,3,14]) {
        cylinder(r=16/2, h=20);
    } 
}

// Servo mount from top bar
difference() {
    translate([90,18.5,22]) { 
        cube([15,7.5,20]);
    }

    // Drill holes for servo mount
    translate([100,18.5+(7.5/2),22+4.5]) {
        rotate([0,90,0])
        cylinder(r=0.75, h=5);
    }
    
    // Drill holes for servo mount
    translate([100,18.5+(7.5/2),22+14.5]) {
        rotate([0,90,0])
        cylinder(r=0.75, h=5);
    }
}


// Top bar support
translate([90,0,15]) {
    rotate([0,90,0]) 
    prism(26,15,15);
}

// Edge support
translate([blackBarWidth/2,11,15]) {
    rotate([0,-90,0]) 
    prism(15,7,20);
}


