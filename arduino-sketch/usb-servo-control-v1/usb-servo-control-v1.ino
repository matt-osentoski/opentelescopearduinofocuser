#include <Servo.h> 

Servo myservo;
int pos = 90; // Speed control.  0 is fastest and 90 is the slowest
String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete
int pwmPin = 9;

void setup() {
  // initialize serial:
  Serial.begin(9600);
  // reserve 200 bytes for the inputString:
  inputString.reserve(200);
}

void loop() {
  
  // print the string when a newline arrives:
  if (stringComplete) {
    
    //First, we need to split the serial string into it's components
    String speed = "22";
    String direction = inputString;
    
    int delim = inputString.indexOf(':');
    if (delim != -1) {
      speed = inputString.substring(0,delim);
      Serial.println(speed);
      direction = inputString.substring(delim+1);
      Serial.println(direction);
    }
    int speedInt = speed.toInt();
    
    Serial.print(inputString);
    if (direction == "left\n") {
      myservo.attach(pwmPin);
      myservo.writeMicroseconds(1700);
      delay(speedInt);
      myservo.detach();
    } 
    if (direction == "right\n") {
      myservo.attach(pwmPin);
      myservo.writeMicroseconds(1300);
      delay(speedInt);
      myservo.detach();
    } 
    // clear the string:
    inputString = "";
    stringComplete = false;
  }
  
}

/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read(); 
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    } 
  }
}
