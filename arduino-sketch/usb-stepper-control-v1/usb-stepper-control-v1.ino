/*-----( Import needed libraries )-----*/
#include <Stepper.h>

/*-----( Declare Constants, Pin Numbers )-----*/
//---( Number of steps per revolution of INTERNAL motor in 4-step mode )---
#define STEPS_PER_MOTOR_REVOLUTION 32

//---( Steps per OUTPUT SHAFT of gear reduction )---
#define STEPS_PER_OUTPUT_REVOLUTION 32 * 64  //2048

int speed = 700; // stepper motor speed
String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete

//The pin connections need to be 4 pins connected
// to Motor Driver In1, In2, In3, In4  and then the pins entered
// here in the sequence 1-3-2-4 for proper sequencing
Stepper small_stepper(STEPS_PER_MOTOR_REVOLUTION, 8, 10, 9, 11);


void setup() {
  // initialize serial:
  Serial.begin(9600);
  // reserve 200 bytes for the inputString:
  inputString.reserve(200);
}

void loop() {
  
  // print the string when a newline arrives:
  if (stringComplete) {
    
    //First, we need to split the serial string into it's components
    String stepsString = "5";
    String direction = inputString;
    
    int delim = inputString.indexOf(':');
    if (delim != -1) {
      stepsString = inputString.substring(0,delim);
      Serial.println(stepsString);
      direction = inputString.substring(delim+1);
      Serial.println(direction);
    }
    int steps = stepsString.toInt();
    
    Serial.print(inputString);
    if (direction == "left\n") {
       small_stepper.setSpeed(speed);
       small_stepper.step(-steps);
    } 
    if (direction == "right\n") {
      small_stepper.setSpeed(speed);
      small_stepper.step(steps);
    } 
    // clear the string:
    inputString = "";
    stringComplete = false;
  }
  
}

/*
  SerialEvent occurs whenever a new data comes in the
 hardware serial RX.  This routine is run between each
 time loop() runs, so using delay inside loop can delay
 response.  Multiple bytes of data may be available.
 */
void serialEvent() {
  while (Serial.available()) {
    // get the new byte:
    char inChar = (char)Serial.read(); 
    // add it to the inputString:
    inputString += inChar;
    // if the incoming character is a newline, set a flag
    // so the main loop can do something about it:
    if (inChar == '\n') {
      stringComplete = true;
    } 
  }
}
