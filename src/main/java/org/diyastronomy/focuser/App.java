package org.diyastronomy.focuser;

import jssc.SerialPort;
import jssc.SerialPortException;
import jssc.SerialPortList;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Hello world!
 *
 */
public class App extends JFrame {

    private SerialPort serialPort;
    private JSlider servoSpeedSlider;
    private String[] ports;
    private JTextArea consoleTextArea;
    private JLabel connectionIndicator;
    private JButton portButton;
    private JScrollPane consoleScrollTextArea;
    private JScrollBar verticalBar;


    public App() {
        init();
    }

    private void init() {

        ports = SerialPortList.getPortNames();
        initUI();
        appendConsole("Ready to connect");
    }

    private void initSerial(String port) {
        // Initalize the serial port settings
        serialPort = new SerialPort(port);
        try {
            serialPort.openPort();//Open serial port
            serialPort.setParams(SerialPort.BAUDRATE_9600,
                    SerialPort.DATABITS_8,
                    SerialPort.STOPBITS_1,
                    SerialPort.PARITY_NONE);//Set params. Also you can set params by this string: serialPort.setParams(9600, 8, 1, 0);

            // NOTE: When you set DTR / RTS to the arduino, it restarts.  A small wait period is therefore needed.
            Thread.sleep(2000);
            connectionIndicator.setForeground(Color.GREEN);
            appendConsole("Serial connection established: " + serialPort.getPortName());
            portButton.setText("Disconnect");
        }
        catch (SerialPortException ex) {
            appendConsole(ex.getMessage());
            System.out.println(ex);
        } catch (InterruptedException e) {
            appendConsole(e.getMessage());
            e.printStackTrace();
        }
    }

    private void initUI() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        JPanel buttonPanel = new JPanel();
        buttonPanel.setMaximumSize(new Dimension(500, 80));

        JPanel connectionPanel = new JPanel();
        connectionPanel.setMaximumSize(new Dimension(800, 80));

        final JComboBox portList = new JComboBox(ports);
        portList.setMaximumSize(new Dimension(200, 40));
        portButton = new JButton("Connect");

        JButton counterClockwiseButton = new JButton("Counter Clockwise");
        JButton clockwiseButton = new JButton("Clockwise");
        servoSpeedSlider = new JSlider(JSlider.HORIZONTAL, 22, 500, 22);
        servoSpeedSlider.setMajorTickSpacing(100);
        servoSpeedSlider.setMinorTickSpacing(10);
        servoSpeedSlider.setPaintTicks(true);
        servoSpeedSlider.setPaintLabels(true);

        connectionIndicator = new JLabel("•");
        connectionIndicator.setForeground(Color.RED);

        consoleTextArea = new JTextArea();
        consoleScrollTextArea = new JScrollPane(consoleTextArea);
        verticalBar = consoleScrollTextArea.getVerticalScrollBar();
        verticalBar.setValue(verticalBar.getMaximum());

        clockwiseButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                try {
                    if (serialPort != null && serialPort.isOpened() == true) {
                        serialPort.writeString(servoSpeedSlider.getValue() + ":right\n");
                        appendConsole("Moving clockwise: " + servoSpeedSlider.getValue());
                    }
                } catch (SerialPortException e) {
                    appendConsole(e.getMessage());
                    e.printStackTrace();
                }
            }
        });

        counterClockwiseButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                    try {
                        if (serialPort != null && serialPort.isOpened() == true) {
                            serialPort.writeString(servoSpeedSlider.getValue() + ":left\n");
                            appendConsole("Moving counter-clockwise: " + servoSpeedSlider.getValue());
                        }
                    } catch (SerialPortException e) {
                        appendConsole(e.getMessage());
                        e.printStackTrace();
                    }
            }
        });

        portButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                // Reinitialize with the new port, but first close existing port
                try {
                    if (serialPort != null && serialPort.isOpened() == true) {
                        appendConsole("closing the serial port: " + serialPort.getPortName());
                        serialPort.closePort(); //Close serial port)
                        connectionIndicator.setForeground(Color.RED);
                        portButton.setText("Connect");
                    } else {
                        initSerial((String)portList.getSelectedItem());
                    }
                } catch (SerialPortException e1) {
                    appendConsole(e1.getMessage());
                    e1.printStackTrace();
                }
                //initSerial((String)portList.getSelectedItem());
            }
        });

        add(panel);
        // Swing button layout
        buttonPanel.add(clockwiseButton);
        buttonPanel.add(counterClockwiseButton);
        panel.add(buttonPanel);
        panel.add(servoSpeedSlider);
        panel.add(Box.createRigidArea(new Dimension(5,50))); // vertical spacer
        connectionPanel.add(portList);
        connectionPanel.add(portButton);
        connectionPanel.add(connectionIndicator);
        panel.add(connectionPanel);
        panel.add(Box.createRigidArea(new Dimension(5,25))); // vertical spacer
        panel.add(consoleScrollTextArea);

        // Create the frame
        setTitle("Arduino Focuser");
        setSize(640, 450);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        appendConsole("Initializing...");
    }

    private void runApp() {
        EventQueue.invokeLater(new Runnable() {

            public void run() {
                setVisible(true);
                addWindowListener(new java.awt.event.WindowAdapter() {
                    public void windowClosing(WindowEvent winEvt) {
                        if (serialPort != null && serialPort.isOpened()) {
                            try {
                                serialPort.closePort();//Close serial port)
                            } catch (SerialPortException e) {
                                e.printStackTrace();
                            }
                        }
                        System.exit(0);
                    }
                });

            }
        });
    }

    private void appendConsole(String consoleText) {
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
        String dateFormat = dt.format(new Date());
        consoleTextArea.append(dateFormat + " " + consoleText + "\n");
        verticalBar.setValue(verticalBar.getMaximum());
    }

    public static void main( String[] args ) {
        App appFrame = new App();
        appFrame.runApp();
    }
}
