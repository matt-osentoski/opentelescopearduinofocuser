# Arduino Focuser
This app connects to an arduino device and controls a servo, using your computer's USB port.  The COM port 
and distance of the servo movement are adjustable.  

## Loading the Sketch into Arduino
Use the included sketch (arduino-sketch/usb-servo-control-v1.ino), to load the required code
into your Arduino device.

## Building the Jar with dependencies
To build an executable Jar with the JSSC dependencies, use the following Maven command:
mvn clean compile assembly:single

## Drivers
Make sure your computer has all the appropriate drivers required to communicate to your Arduino device 
using the USB port.  Test that you can load the sketch mentioned above, using the Arduino IDE to ensure the 
USB port is setup correctly.  
  
For example, if you're using an FTDI chip (highly recommended), then the following URL contains the drivers:
http://www.ftdichip.com/Drivers/VCP.htm
